﻿CREATE TYPE [dbo].[TTCharacterClaim] AS TABLE (
    [Id]   INT          NULL,
    [Name] VARCHAR (50) NULL);

