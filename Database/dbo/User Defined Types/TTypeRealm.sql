﻿CREATE TYPE [dbo].[TTypeRealm] AS TABLE (
    [Id]   INT          NULL,
    [Name] VARCHAR (50) NULL,
    [Type] VARCHAR (50) NULL);

