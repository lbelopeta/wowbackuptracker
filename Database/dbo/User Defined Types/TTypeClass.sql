﻿CREATE TYPE [dbo].[TTypeClass] AS TABLE (
    [Id]         INT          NULL,
    [Name]       VARCHAR (50) NULL,
    [ExternalId] INT          NULL);

