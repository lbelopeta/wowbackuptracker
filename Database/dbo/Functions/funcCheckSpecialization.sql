﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[funcCheckSpecialization]
(
	-- Add the parameters for the function here
	@ClassId int,
	@SpecializationId int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsValid bit = 0;

	Select 
		@IsValid = Id 
	from 
		Specialization 
	where 
		ClassId = @ClassId and
		Id = @SpecializationId;

	RETURN @IsValid;

END