﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[funcCheckOptionalSpecialization]
(
	-- Add the parameters for the function here
	@ClassId int,
	@SpecializationId int 
	)
RETURNS bit
AS
BEGIN
	DECLARE @IsValid bit = 0;
	
	 if(@SpecializationId is null)
	 begin
		set @IsValid = 1;
		RETURN @IsValid;
		end
	 else
	 begin
	Select 
		@IsValid = Id 
	from 
		Specialization 
	where 
		ClassId = @ClassId and
		Id = @SpecializationId;
end
	RETURN @IsValid;

END