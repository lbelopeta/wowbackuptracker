﻿
CREATE PROCEDURE spSelGuildMembers
	@GuildId int
AS
BEGIN
	SET NOCOUNT ON;

   Select * from dbo.GuildMembers where GuildId=@GuildId;
END