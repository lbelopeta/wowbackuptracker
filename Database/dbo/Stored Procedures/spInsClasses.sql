﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spInsClasses
	-- Add the parameters for the stored procedure here
	@Classes dbo.TTypeClass readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	merge dbo.Class t
	using(
		select * from @Classes
	) s on t.Name = s.Name
	when not matched by target then
	insert (Name,ExternalId) values(s.Name,s.ExternalId)
	when matched then 
	update 
	set 
	name = s.name,
	externalId = s.externalId;
	return 0;
END