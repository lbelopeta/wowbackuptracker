﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spInsRealm
	-- Add the parameters for the stored procedure here
	@Name varchar(50),
	@Type varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Insert into dbo.Realm (Name,Type) values (@Name,@Type);
	Select * from realm where id = SCOPE_IDENTITY();
	return 0;
END