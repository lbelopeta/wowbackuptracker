﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpInsRealms]
	-- Add the parameters for the stored procedure here
	@Realms dbo.TTypeRealm readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	merge dbo.Realm t
	using(
	select * from @Realms
	) s on t.Name = s.Name
	when not matched by target then
	insert (Name,Type) values(s.Name,s.Type)
	when matched then 
	update 
	set 
	type = s.type;
	return 0;
  
END