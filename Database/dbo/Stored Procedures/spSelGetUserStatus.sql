﻿CREATE PROCEDURE [dbo].[spSelGetUserStatus]
	@UserId int,
	@CharacterId int = null
AS
BEGIN
	SET NOCOUNT ON;

	Declare @IsGuildOwner bit = 0;
	Declare @IsInGuild bit = 0;
	Declare @HasCharacters bit = 0;

	select 
		@HasCharacters = id 
	from
		 Character 
	where 
		UserId = @UserId;

	select 
		@IsGuildOwner = Id 
	from 
		Guild
	where
		OwnerId = @CharacterId;

	if(@IsGuildOwner = 1)
	begin
		set @IsInGuild = 1;
	end
	else
	begin
		select 
			@IsInGuild = Id
		from
			GuildMember
		where 
			CharacterId = @CharacterId and
			DateOfLeave is null;
	end

	select @IsGuildOwner as IsGuildOwner,@IsInGuild as IsInGuild,@HasCharacters as HasCharacters;
END