﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelCharacterClaims]
	@UserId int,
	@CharacterId int = 0
AS
BEGIN
	
	declare @HasCharacter bit = 0;
	if @CharacterId != 0
		set @HasCharacter = 1;
	else
	 select @HasCharacter = id from Character where UserId = @UserId;

	declare @OwnsGuild bit  = 0;

	select @OwnsGuild = Id from Guild where OwnerId = @CharacterId;

	declare @IsInGuild bit = 0;

	if @OwnsGuild = 1 
		set @IsInGuild = 1;
	else
		select @IsInGuild = Id from GuildMember where CharacterId = @CharacterId;

	declare @IsAdministrator bit = 0;
	select @IsAdministrator = Id from dbo.UserRoles where UserId = @UserId and UserRoleId = 
	(select id from dbo.UserRole where Name = 'Administrator');

	declare @Calims TTCharacterClaim;

	
	if @HasCharacter = 1
		insert into @Calims(Id,Name) Select Id,Name from CharacterClaim where Name = 'HasCharacter';

	if @CharacterId != 0
	begin
	if @OwnsGuild = 1
		insert into @Calims(Id,Name) Select Id,Name from CharacterClaim where Name = 'OwnsGuild';

	if @IsInGuild = 1  
		insert into @Calims(Id,Name) Select Id,Name from CharacterClaim where Name = 'IsInGuild';
	else
		insert into @Calims(Id,Name) Select Id,Name from CharacterClaim where Name = 'IsNotInGuild';
	end

	if @IsAdministrator = 1
		insert into @Calims(Id,Name) Select Id,Name from CharacterClaim where Name = 'IsAdministrator';
	select * from @Calims

END