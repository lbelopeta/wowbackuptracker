﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spSelSpecializations
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        Specialization.*, Role.Name AS RoleName, Class.Name AS ClassName 
FROM            Specialization INNER JOIN
                         Role ON Specialization.RoleId = Role.Id INNER JOIN
                         Class ON Specialization.ClassId = Class.Id;
return 0;
END