﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsCharacter]
	-- Add the parameters for the stored procedure here
	@Name varchar(50),
	@ClassId int,
	@PrimarySpecializationId int,
	@SecondarySpecializationId int = null,
	@RealmId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into Character(Name,ClassId,PrimarySpecializationId,SecondarySpecializationId,RealmId,UserId)
	values (@Name,@ClassId,@PrimarySpecializationId,@SecondarySpecializationId,@RealmId,@UserId);
	return 0;
END