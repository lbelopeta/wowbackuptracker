﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelUserActionsForMenu]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT [Id]
      ,[ParentActionId]
      ,[ClaimId]
      ,[Name]
      ,[Action]
      ,[Code]
      ,[GenerateMenuItem]
      ,[Order]
  FROM [dbo].[UserAction]
    END