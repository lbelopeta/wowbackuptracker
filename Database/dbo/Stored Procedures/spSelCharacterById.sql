﻿
CREATE PROCEDURE spSelCharacterById
	@CharacterId int
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from Character where Character.Id = @CharacterId;
END