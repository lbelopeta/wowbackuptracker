﻿
CREATE PROCEDURE spInsGuildApplication 
	-- Add the parameters for the stored procedure here
	@GuildId int,
	@CharacterId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Check bit;
	select @Check = Id from GuildApplications where GuildId = @GuildId  and CharacterId = @CharacterId;

	if(@Check = 1)
		return 0;

	INSERT INTO [dbo].[GuildApplications]
           ([GuildId]
           ,[CharacterId]
           ,[DateOfApplication])
     VALUES
           (@GuildId
           ,@CharacterId

           ,GETDATE()
           );
	return 0;
END