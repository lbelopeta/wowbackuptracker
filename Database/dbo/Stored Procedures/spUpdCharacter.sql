﻿
CREATE PROCEDURE spUpdCharacter 
	@Id int,
	@ClassId int,
	@RealmId int,
	@PrimarySpecializationId int,
	@SecondarySpecializationId int = NULL
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE [dbo].[Character]
		SET
			[ClassId] = @ClassId
			,[PrimarySpecializationId] = @PrimarySpecializationId
			,[SecondarySpecializationId] = @SecondarySpecializationId
			,[RealmId] = @RealmId
		WHERE 
			Id = @Id;

END