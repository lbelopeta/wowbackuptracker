﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelGuildsForApplication]
	@CharacterId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        Guild.Id,
			Guild.Name,
			Case when GuildApplications.Id is null then cast(0 as bit) else Cast(GuildApplications.Id as bit) end as HasApplied 
FROM            Guild LEFT OUTER JOIN
                         GuildApplications ON Guild.Id = GuildApplications.GuildId 
						 and GuildApplications.CharacterId = @CharacterId;
						 
END