﻿CREATE PROCEDURE spSelAllUserCharacters
	@UserId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT        
		Character.Id, 
		Character.Name, 
		Character.ClassId, 
		Specialization.Name AS PrimarySepcializationName,
		SecondarySpecialization.Name AS SecondarySepcializationName, 
		Class.Name AS ClassName, 
		Realm.Name AS RealmName
	FROM            
		Character INNER JOIN
        Class ON Character.ClassId = Class.Id INNER JOIN
        Realm ON Character.RealmId = Realm.Id INNER JOIN
        Specialization ON  Character.PrimarySpecializationId = Specialization.Id AND Class.Id = Specialization.ClassId LEFT JOIN
		Specialization as SecondarySpecialization ON Character.SecondarySpecializationId = SecondarySpecialization.Id AND Class.Id = SecondarySpecialization.ClassId
	where 
		Character.UserId = @UserId;

END