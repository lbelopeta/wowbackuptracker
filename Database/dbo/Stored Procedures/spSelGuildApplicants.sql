﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSelGuildApplicants]
	@GuildId int
AS
BEGIN

  SELECT
		[Id] 
	  ,[CharacterName]
      ,[ClassName]
      ,[PrimarySpecialization]
      ,[SecondarySpecialization]
      ,[RealmName]
      ,[DateOfApplication]
  FROM [BackupTracker].[dbo].[GuildApplicants] where GuildId = @GuildId and [DateOfResponse] is null;

END