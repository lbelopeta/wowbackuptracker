﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsUser]	
	@UserName varchar(50) ,
	@Password char(44),
	@Email varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @usernameInUse as bit;
	declare @UserId int;
	declare @ErrorMessage varchar(250);
    -- Insert statements for procedure here
	begin try

	begin tran t1;
	insert into [BackupTracker].[dbo].[User] (username,password,email) values(@UserName,@Password,@Email);
	set @UserID = Scope_Identity();
	declare @RoleId int = 0;
	select @RoleId = id from userrole where name = 'Usr';
	if(@RoleId = 0)
	begin
		rollback tran t1;
		set @ErrorMessage = 'Error. No help for you.';
		; throw 60000,'Error',1;
	end
	insert into UserRoles (UserId,UserRoleId) values (@userID,@RoleId);

	commit tran t1;
	select * from [BackupTracker].[dbo].[User] where id = @UserId;
	return 0;
	end try
	begin catch 
		RAISERROR(@ErrorMessage,16,1)
	end catch
	
END