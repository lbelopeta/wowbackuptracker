﻿
CREATE PROCEDURE [dbo].[spInsGuild]
	@Name varchar(50),
	@RealmId int,
	@OwnerId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    begin try
	begin tran t1;
		declare @HasGuild bit;
		select @HasGuild = Id from guild where OwnerId = @OwnerId;

		if(@HasGuild = 1)
		begin
			;throw 600001,'User already owns guild',1;
		end

		declare @IsInGuild bit;
		select @IsInGuild = Id from GuildMember where CharacterId = @OwnerId;

		if(@IsInGuild = 1)
		begin
			;throw 600002,'User is already in a guild',1;
		end

		--remove all applications for character
		update GuildApplications 
		set	Accepted = 0
		where CharacterId = @OwnerId;

		INSERT INTO [dbo].[Guild]
           ([Name]
           ,[RealmId]
           ,[OwnerId])
		VALUES
           (@Name
           ,@RealmId
           ,@OwnerId);

		declare @GuildId int;
		set @GuildId = SCOPE_IDENTITY();

		INSERT INTO [dbo].[GuildMember]
				([GuildId]
				,[CharacterId]
				,[DateOfJoining])
		 VALUES
				 (@GuildId
				,@OwnerId
				,GETDATE()
				);

	commit tran t1;

	select * from Guild where id = @GuildId;
	end try
	begin catch
		rollback tran t1;
		return 1;
	end catch
	return 0;
END