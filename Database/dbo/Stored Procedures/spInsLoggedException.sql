﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spInsLoggedException]
	@Type varchar(50),
	@Message text,
	@Parameters text,
	@StackTrace text,
	@Source varchar(50),
	@TargetSite varchar(50)
AS
BEGIN
set nocount ON;
	insert into LoggedException (Type,Message,Parameters,StackTrace,Source,TargetSite,Date)
	values (@Type,@Message,@Parameters,@StackTrace,@Source,@TargetSite,GETDATE());

 select * from LoggedException where
	Id = SCOPE_IDENTITY();

END