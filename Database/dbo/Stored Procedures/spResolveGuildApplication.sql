﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spResolveGuildApplication] 
	@ApplicationId int,
	@Accepted bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Begin try
	begin tran t1;
	if @Accepted = 0 
		update GuildApplications  set
			Accepted = 0,
			DateOfResponse = GETDATE()
		where
		Id = @ApplicationId
	else 
	begin
	--clear all other applications
	declare @CharacterId int = 0;
	declare @GuildId int = 0;

	select 
		@CharacterId = CharacterId,
		@GuildId = GuildId
	from
	GuildApplications
	where 
	id =@ApplicationId;

	if @CharacterId = 0 or @GuildId = 0
	throw 50000,'Something wrong with application',1;

	update GuildApplications  set
			Accepted = 0
		where
		CharacterId = @CharacterId and
		GuildId != @GuildId;

	update GuildApplications  set
			Accepted = 1,
			DateOfResponse = GETDATE()
		where
		id = @ApplicationId

	INSERT INTO [dbo].[GuildMember]
           ([GuildId]
           ,[CharacterId]
           ,[DateOfJoining]
           ,[DateOfLeave])
     VALUES
           (@GuildId
           ,@CharacterId
           ,GETDATE()
           ,NULL)
	end


	commit tran t1;
	end try
	begin catch
	rollback tran t1;
	end catch;
END