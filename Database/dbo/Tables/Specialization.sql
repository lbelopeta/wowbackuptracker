﻿CREATE TABLE [dbo].[Specialization] (
    [Id]      INT          IDENTITY (1, 1) NOT NULL,
    [Name]    VARCHAR (50) NOT NULL,
    [ClassId] INT          NOT NULL,
    [RoleId]  INT          NOT NULL,
    CONSTRAINT [PK_Specialization] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Specialization_Class] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_Specialization_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id])
);



