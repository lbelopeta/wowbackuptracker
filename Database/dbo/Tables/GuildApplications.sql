﻿CREATE TABLE [dbo].[GuildApplications] (
    [Id]                INT      IDENTITY (1, 1) NOT NULL,
    [GuildId]           INT      NOT NULL,
    [CharacterId]       INT      NOT NULL,
    [Accepted]          BIT      NULL,
    [DateOfApplication] DATETIME CONSTRAINT [DF_GuildApplications_DateOfApplication] DEFAULT (getdate()) NOT NULL,
    [DateOfResponse]    DATETIME NULL,
    CONSTRAINT [PK_GuildApplications] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_GuildApplications_Character] FOREIGN KEY ([CharacterId]) REFERENCES [dbo].[Character] ([Id]),
    CONSTRAINT [FK_GuildApplications_Guild] FOREIGN KEY ([GuildId]) REFERENCES [dbo].[Guild] ([Id])
);

