﻿CREATE TABLE [dbo].[User] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Username]  VARCHAR (50)  NOT NULL,
    [Password]  CHAR (44)     NOT NULL,
    [BattleTag] VARCHAR (50)  NULL,
    [Email]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC)
);



