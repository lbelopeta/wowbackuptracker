﻿CREATE TABLE [dbo].[GuildMember] (
    [Id]            INT      IDENTITY (1, 1) NOT NULL,
    [GuildId]       INT      NOT NULL,
    [CharacterId]   INT      NOT NULL,
    [DateOfJoining] DATETIME CONSTRAINT [DF_GuildMember_DateOfJoining] DEFAULT (getdate()) NOT NULL,
    [DateOfLeave]   DATETIME NULL,
    CONSTRAINT [PK_GuildMember] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_GuildMember_Character] FOREIGN KEY ([CharacterId]) REFERENCES [dbo].[Character] ([Id]),
    CONSTRAINT [FK_GuildMember_Guild] FOREIGN KEY ([GuildId]) REFERENCES [dbo].[Guild] ([Id])
);

