﻿CREATE TABLE [dbo].[LoggedException] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [Type]        VARCHAR (50)     NOT NULL,
    [Message]     TEXT             NOT NULL,
    [Parameters]  TEXT             NOT NULL,
    [StackTrace]  TEXT             NOT NULL,
    [Source]      VARCHAR (50)     NOT NULL,
    [TargetSite]  VARCHAR (50)     NOT NULL,
    [Date]        DATETIME         NOT NULL,
    [ErrorNumber] UNIQUEIDENTIFIER CONSTRAINT [DF_LoggedException_ErrorNumber] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_LoggedException] PRIMARY KEY CLUSTERED ([Id] ASC)
);



