﻿CREATE TABLE [dbo].[Realm] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    [Type] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Realm] PRIMARY KEY CLUSTERED ([Id] ASC)
);



