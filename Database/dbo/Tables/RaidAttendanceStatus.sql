﻿CREATE TABLE [dbo].[RaidAttendanceStatus] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_RaidAttendanceStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

