﻿CREATE TABLE [dbo].[Character] (
    [Id]                        INT          IDENTITY (1, 1) NOT NULL,
    [Name]                      VARCHAR (50) NOT NULL,
    [ClassId]                   INT          NOT NULL,
    [PrimarySpecializationId]   INT          NOT NULL,
    [SecondarySpecializationId] INT          NULL,
    [UserId]                    INT          NOT NULL,
    [RealmId]                   INT          NOT NULL,
    CONSTRAINT [PK_Character] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [checkPrimarySpecialization] CHECK ([dbo].[funcCheckSpecialization]([ClassId],[PrimarySpecializationId])=(1)),
    CONSTRAINT [checkSecondarySpecialization] CHECK ([dbo].[funcCheckOptionalSpecialization]([ClassId],[PrimarySpecializationId])=(1)),
    CONSTRAINT [FK_Character_Class] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_Character_Realm] FOREIGN KEY ([RealmId]) REFERENCES [dbo].[Realm] ([Id]),
    CONSTRAINT [FK_Character_Specialization] FOREIGN KEY ([PrimarySpecializationId]) REFERENCES [dbo].[Specialization] ([Id]),
    CONSTRAINT [FK_Character_Specialization1] FOREIGN KEY ([SecondarySpecializationId]) REFERENCES [dbo].[Specialization] ([Id]),
    CONSTRAINT [FK_Character_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);





