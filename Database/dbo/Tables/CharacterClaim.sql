﻿CREATE TABLE [dbo].[CharacterClaim] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_CharacterClaim] PRIMARY KEY CLUSTERED ([Id] ASC)
);

