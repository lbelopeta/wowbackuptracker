﻿CREATE TABLE [dbo].[RaidInvite] (
    [Id]                 INT IDENTITY (1, 1) NOT NULL,
    [RaidId]             INT NOT NULL,
    [GuildMemberId]      INT NOT NULL,
    [Accepted]           BIT NULL,
    [AttendanceStatusId] INT NULL,
    CONSTRAINT [PK_RaidInvite] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RaidInvite_GuildMember] FOREIGN KEY ([GuildMemberId]) REFERENCES [dbo].[GuildMember] ([Id]),
    CONSTRAINT [FK_RaidInvite_Raid] FOREIGN KEY ([RaidId]) REFERENCES [dbo].[Raid] ([Id]),
    CONSTRAINT [FK_RaidInvite_RaidAttendanceStatus] FOREIGN KEY ([AttendanceStatusId]) REFERENCES [dbo].[RaidAttendanceStatus] ([Id])
);

