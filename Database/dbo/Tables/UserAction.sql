﻿CREATE TABLE [dbo].[UserAction] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [ParentActionId]   INT           NULL,
    [ClaimId]          INT           NULL,
    [Name]             VARCHAR (MAX) NOT NULL,
    [Action]           VARCHAR (MAX) NOT NULL,
    [Code]             VARCHAR (MAX) NOT NULL,
    [GenerateMenuItem] BIT           NOT NULL,
    [Order]            INT           NOT NULL,
    CONSTRAINT [PK_UserAction] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserAction_CharacterClaim] FOREIGN KEY ([ClaimId]) REFERENCES [dbo].[CharacterClaim] ([Id]),
    CONSTRAINT [FK_UserAction_UserAction] FOREIGN KEY ([ParentActionId]) REFERENCES [dbo].[UserAction] ([Id])
);



