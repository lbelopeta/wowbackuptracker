﻿CREATE TABLE [dbo].[Guild] (
    [Id]      INT          IDENTITY (1, 1) NOT NULL,
    [Name]    VARCHAR (50) NOT NULL,
    [RealmId] INT          NOT NULL,
    [OwnerId] INT          NOT NULL,
    CONSTRAINT [PK_Guild] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Guild_Character] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Character] ([Id]),
    CONSTRAINT [FK_Guild_Realm] FOREIGN KEY ([RealmId]) REFERENCES [dbo].[Realm] ([Id])
);



