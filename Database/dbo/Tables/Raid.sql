﻿CREATE TABLE [dbo].[Raid] (
    [Id]              INT      IDENTITY (1, 1) NOT NULL,
    [GuildId]         INT      NOT NULL,
    [Date]            DATETIME NOT NULL,
    [NumberOfTanks]   INT      NOT NULL,
    [NumberOfDPS]     INT      NOT NULL,
    [NumberOfHealers] INT      NOT NULL,
    CONSTRAINT [PK_Raid] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Raid_Guild] FOREIGN KEY ([GuildId]) REFERENCES [dbo].[Guild] ([Id])
);

