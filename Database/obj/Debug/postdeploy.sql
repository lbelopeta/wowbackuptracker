﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*insert roles*/
 merge dbo.Role t
using(
	select 'Tank' as Name 
	union
	select 'Healer' 
	union
	select  'DPS' 
) s on t.[Name] = s.[Name]
when not matched by target then
insert([Name]) values(s.[Name]);

/*insert specializations*/

merge dbo.specialization t
using (
	select 'Arms' as Name, (Select Id from Class where name = 'Warrior') as ClassId,(Select Id From role where name = 'DPS' ) as RoleId
	union
	select 'Fury', (Select Id from Class where name = 'Warrior'),(Select Id From role where name = 'DPS' )
		union
	select 'Protection' ,(Select Id from Class where name = 'Warrior'),(Select Id From role where name = 'Tank' )
	union 
	select 'Holy' ,(Select Id from Class where name = 'Paladin'),(Select Id From role where name = 'Healer' )
	union
	select 'Retribtuion' ,(Select Id from Class where name = 'Paladin'),(Select Id From role where name = 'DPS' )
	union
	select 'Protection' ,(Select Id from Class where name = 'Paladin'),(Select Id From role where name = 'Tank' )
	union
	select 'Beast Mastery' ,(Select Id from Class where name = 'Hunter'),(Select Id From role where name = 'DPS' )
	union
	select 'Marksmanship' ,(Select Id from Class where name = 'Hunter'),(Select Id From role where name = 'DPS' )
	union
	select 'Survival' ,(Select Id from Class where name = 'Hunter'),(Select Id From role where name = 'DPS' )
	union 
	select 'Assassination' ,(Select Id from Class where name = 'Rogue'),(Select Id From role where name = 'DPS' )
	union
	select 'Outlaw' ,(Select Id from Class where name = 'Rogue'),(Select Id From role where name = 'DPS' )
	union
	select 'Subtlety' ,(Select Id from Class where name = 'Rogue'),(Select Id From role where name = 'DPS' )
	union
	select 'Discipline' ,(Select Id from Class where name = 'Priest'),(Select Id From role where name = 'Healer' )
	union
	select 'Holy' ,(Select Id from Class where name = 'Priest'),(Select Id From role where name = 'Healer' )
	union
	select 'Shadow' ,(Select Id from Class where name = 'Priest'),(Select Id From role where name = 'DPS' )
	union
	select 'Frost' ,(Select Id from Class where name = 'Death Knight'),(Select Id From role where name = 'DPS' )
	union
	select 'Unholy' ,(Select Id from Class where name = 'Death Knight'),(Select Id From role where name = 'DPS' )
	union
	select 'Blood' ,(Select Id from Class where name = 'Death Knight'),(Select Id From role where name = 'Tank' )
	union
	select 'Elemental' ,(Select Id from Class where name = 'Shaman'),(Select Id From role where name = 'DPS' )
	union
	select 'Enhancment' ,(Select Id from Class where name = 'Shaman'),(Select Id From role where name = 'DPS' )
	union
	select 'Restoration' ,(Select Id from Class where name = 'Shaman'),(Select Id From role where name = 'Healer' )
	union
	select 'Arcane' ,(Select Id from Class where name = 'Mage'),(Select Id From role where name = 'DPS' )
	union
	select 'Fire' ,(Select Id from Class where name = 'Mage'),(Select Id From role where name = 'DPS' )
	union
	select 'Frost' ,(Select Id from Class where name = 'Mage'),(Select Id From role where name = 'DPS' )
	union
	select 'Affliction' ,(Select Id from Class where name = 'Warlock'),(Select Id From role where name = 'DPS' )
	union
	select 'Demonology' ,(Select Id from Class where name = 'Warlock'),(Select Id From role where name = 'DPS' )
	union
	select 'Destruction' ,(Select Id from Class where name = 'Warlock'),(Select Id From role where name = 'DPS' )
	union
	select 'Brewmaster' ,(Select Id from Class where name = 'Monk'),(Select Id From role where name = 'Tank' )
	union
	select 'Mistweaver' ,(Select Id from Class where name = 'Monk'),(Select Id From role where name = 'Healer' )
	union
	select 'Windwalker' ,(Select Id from Class where name = 'Monk'),(Select Id From role where name = 'DPS' )
	union
	select 'Balance' ,(Select Id from Class where name = 'Druid'),(Select Id From role where name = 'DPS' )
	union
	select 'Feral' ,(Select Id from Class where name = 'Druid'),(Select Id From role where name = 'DPS' )
	union
	select 'Guardian' ,(Select Id from Class where name = 'Druid'),(Select Id From role where name = 'Tank' )
	union
	select 'Restoration' ,(Select Id from Class where name = 'Druid'),(Select Id From role where name = 'Healer' )
	union
	select 'Havoc' ,(Select Id from Class where name = 'Demon Hunter'),(Select Id From role where name = 'DPS' )
	union
	select 'Vengance' ,(Select Id from Class where name = 'Demon Hunter'),(Select Id From role where name = 'Tank' )

) s on t.name = s.name
when not matched by target then
insert (name,classid,roleid) values(s.name,s.classid,s.roleid)
when matched then 
update set
classId = s.classid,
roleid = s.roleId;

/* fill attendance status table*/
merge [dbo].[RaidAttendanceStatus] t
using (
	select 'In' as name
	union
	select 'Out (Declined)'
	union 
	select 'Backup'
) s on t.name = s.name
when not matched by target then
insert (name) values(s.name);

/* add check constraints to character table*/
alter table Character
add constraint checkPrimarySpecialization
check(dbo.funcCheckSpecialization(ClassId,PrimarySpecializationId) = 1)

alter table Character
add constraint checkSecondarySpecialization
check([dbo].[funcCheckOptionalSpecialization](ClassId,PrimarySpecializationId) = 1)

/*add user roles*/
merge UserRole t
using
(
	select 'Administrator' as Name
	union
	select 'User'
)s on t.Name = s.Name
when not matched by target then
	insert (Name) values (s.Name);

/*create root action for administration*/
merge userAction t
using
(
	select	null as ParentActionID,'Administration' as Name,'/Administration/Index' as Action,'Administration_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add childer actions for administration*/
	merge userAction t
	using
	(
		select	(select id from userAction where code = 'Administration_Index') as ParentActionID,'Administration Import Realms' as Name,'/Administration/ImportRealms' as Action,
		'Administration_ImportRealms' as Code,0 as GenerateMenuItem,1 as [Order]
		union
		select	(select id from userAction where code = 'Administration_Index') as ParentActionID,'Administration Import Classes' as Name,'/Administration/ImportClasses' as Action,
		'Administration_ImportClasses' as Code,0 as GenerateMenuItem,1 as [Order]
	)s on t.Code = s.Code
	when not matched by target then
	insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
	when matched then
	update
		set
			Name = s.Name,
			GenerateMenuItem = s.GenerateMenuItem,
			[Order] = s.[Order];

/*add root action for character*/
merge userAction t
using
(
	select	null as ParentActionID,'Chracter' as Name,'/Character/Index' as Action,'Character_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/* add childer actions for character*/
merge userAction t
	using
	(
		select	(select id from userAction where code = 'Character_Index') as ParentActionID,'Character Create' as Name,'/Character/Create' as Action,
		'Character_Create' as Code,0 as GenerateMenuItem,1 as [Order]
		union
		select	(select id from userAction where code = 'Character_Index') as ParentActionID,'Character Edit' as Name,'/Character/Edit' as Action,
		'Character_Edit' as Code,0 as GenerateMenuItem,1 as [Order]
		union 
		select	(select id from userAction where code = 'Character_Index') as ParentActionID,'Character GetSpecializationsForClass' as Name,'/Character/GetSpecializationsForClass' as Action,
		'Character_GetSpecializationsForClass' as Code,0 as GenerateMenuItem,1 as [Order]
	)s on t.Code = s.Code
	when not matched by target then
	insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
	when matched then
	update
		set
			Name = s.Name,
			GenerateMenuItem = s.GenerateMenuItem,
			[Order] = s.[Order];

/*add root action for class*/
merge userAction t
using
(
	select	null as ParentActionID,'Class' as Name,'/Class/Index' as Action,'Class_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add root action for guild application*/
merge userAction t
using
(
	select	null as ParentActionID,'Guild Application' as Name,'/GuildApplication/Index' as Action,'GuildApplication_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add child actions for guild application*/
	merge userAction t
	using
	(
		select	(select id from userAction where code = 'GuildApplication_Index') as ParentActionID,'Guild application apply' as Name,'/GuildApplication/Apply' as Action,
		'GuildApplication_Apply' as Code,0 as GenerateMenuItem,1 as [Order]
		
	)s on t.Code = s.Code
	when not matched by target then
	insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
	when matched then
	update
		set
			Name = s.Name,
			GenerateMenuItem = s.GenerateMenuItem,
			[Order] = s.[Order];

/* add root action for guild*/
merge userAction t
using
(
	select	null as ParentActionID,'Guild' as Name,'/Guild/Index' as Action,'Guild_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add child actions for guild*/
	merge userAction t
	using
	(
		select	(select id from userAction where code = 'GuildIndex') as ParentActionID,'Guild create' as Name,'/Guild/Create' as Action,
		'Guild_Create' as Code,0 as GenerateMenuItem,1 as [Order]
		
	)s on t.Code = s.Code
	when not matched by target then
	insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
	when matched then
	update
		set
			Name = s.Name,
			GenerateMenuItem = s.GenerateMenuItem,
			[Order] = s.[Order];

/*add root action for realm*/
merge userAction t
using
(
	select	null as ParentActionID,'Realm' as Name,'/Realm/Index' as Action,'Realm_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add root action for specialization*/
merge userAction t
using
(
	select	null as ParentActionID,'Specialization' as Name,'/Specialization/Index' as Action,'Specialization_Index' as Code,1 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order]) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order])
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*fill claims*/
merge CharacterClaim t 
using
(
	select 'HasCharacter' as Name
	union
	select 'IsInGuild'
	union
	select 'OwnsGuild'
	union
	select 'IsNotInGuild'
	union
	select 'IsAdministrator'
) s
on t.Name = s.Name
when not matched by target then
insert (Name) values (s.Name);

merge UserAction t
using(
	select id, (select id from characterclaim where name = 'HasCharacter') as ClaimId from UserAction where code = 'Character_Index'
	union
	select id, (select id from characterclaim where name = 'HasCharacter') as ClaimId from UserAction where code = 'Character_Edit'
	union
	select id, (select id from characterclaim where name = 'IsNotInGuild') as ClaimId from UserAction where code = 'GuildApplication_Index'
	union
	select id, (select id from characterclaim where name = 'IsNotInGuild') as ClaimId from UserAction where code = 'GuildApplication_Apply'
	union
	select id, (select id from characterclaim where name = 'IsInGuild') as ClaimId from UserAction where code = 'Guild_Index'
	union
	select id, (select id from characterclaim where name = 'IsNotInGuild') as ClaimId from UserAction where code = 'Guild_Create'
	union
	select id, (select id from characterclaim where name = 'IsAdministrator') as ClaimId from UserAction where code = 'Administration_Index'
		union
	select id, (select id from characterclaim where name = 'IsAdministrator') as ClaimId from UserAction where code = 'Administration_ImportRealms'
		union
	select id, (select id from characterclaim where name = 'IsAdministrator') as ClaimId from UserAction where code = 'Administration_ImportClasses'
) s on t.id = s.id
when matched  then
update
	set ClaimId = s.ClaimId;

	/* add action for home*/
merge userAction t
using
(
	select	null as ParentActionID,'Home' as Name,'/Home/Index' as Action,'Home_Index' as Code,0 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order],ClaimId) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order],null)
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add action for change of character*/
merge userAction t
using
(
	select	null as ParentActionID,'Character change selected' as Name,'/Character/ChangeSelectedCharacter' as Action,'Character_ChangeSelectedCharacter' as Code,0 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order],ClaimId) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order],null)
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add action for log off*/
merge userAction t
using
(
	select	null as ParentActionID,'Account Log off' as Name,'/Account/LogOff' as Action,'Account_LogOff' as Code,0 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order],ClaimId) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order],null)
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add root action for guild applicants*/
merge userAction t
using
(
	select	null as ParentActionID,'Guild applicants' as Name,'/GuildApplicants/Index' as Action,'GuildApplicants_Index' as Code,0 as GenerateMenuItem,1 as [Order]
)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order],ClaimId) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order], (select id from CharacterClaim where name = 'OwnsGuild'))
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		[Order] = s.[Order];

/*add actions for guild applicants*/
merge userAction t
using
(
	select	(select id from userAction where code = 'GuildApplicants_Index') as ParentActionID,'Guild applicants accept' as Name,'/GuildApplicants/Accept' as Action,'GuildApplicants_Accept' as Code,0 as GenerateMenuItem,1 as [Order]
	union
	select	(select id from userAction where code = 'GuildApplicants_Index') as ParentActionID,'Guild applicants Decline' as Name,'/GuildApplicants/Decline' as Action,'GuildApplicants_Decline' as Code,0 as GenerateMenuItem,1 as [Order]

)s on t.Code = s.Code
when not matched by target then
insert(ParentActionID,Name,Action,Code,GenerateMenuItem,[Order],ClaimId) values(s.ParentActionID,s.Name,s.Action,s.Code,s.GenerateMenuItem,s.[Order], (select id from CharacterClaim where name = 'OwnsGuild'))
when matched then
update
	set
		Name = s.Name,
		GenerateMenuItem = s.GenerateMenuItem,
		ParentActionId = s.ParentActionID,
		[Order] = s.[Order];

GO
