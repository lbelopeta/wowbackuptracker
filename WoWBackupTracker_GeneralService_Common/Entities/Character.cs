﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class Character
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int ClassId { get; set; }
        [DataMember]
        public int RealmId { get; set; }
        [DataMember]
        public int PrimarySpecializationId { get; set; }
        [DataMember]
        public int? SecondarySpecializationId { get; set; }
        [DataMember]
        public int UserId { get; set; }
    }
}
