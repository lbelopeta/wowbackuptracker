﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class GuildApplicant
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string CharacterName { get; set; }

        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public string PrimarySpecialization { get; set; }

        [DataMember]
        public string SecondarySpecialization { get; set; }

        [DataMember]
        public string RealmName { get; set; }

        [DataMember]
        public DateTime DateOfApplication { get; set; }
    }
}
