﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    public class CharacterGridViewModel : Character
    {
        [DataMember]
        public string ClassName { get; set; }
        [DataMember]
        public string RealmName { get; set; }
        [DataMember]
        public string PrimarySepcializationName { get; set; }
        [DataMember]
        public string SecondarySepcializationName { get; set; }
    }
}
