﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    public class SpecializationGridViewModel : Specialization
    {
        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public string RoleName { get; set; }
    }
}
