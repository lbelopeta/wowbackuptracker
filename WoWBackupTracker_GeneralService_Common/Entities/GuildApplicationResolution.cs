﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class GuildApplicationResolution
    {
        [DataMember]
        public int ApplicationId { get; set; }

        [DataMember]
        public bool Accepted { get; set; }
    }
}
