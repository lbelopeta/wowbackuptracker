﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WoWBT_Common.ExceptionHandling;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class LoggedException
    {
        [DataMember]
        public string Parameters { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string TargetSite { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public Guid ErrorNumber { get; set; }

        public LoggedException(MetaException exception)
        {
            Parameters = exception.Parameters;
            Message = exception.Message;
            StackTrace = exception.StackTrace;
            Source = exception.Source;
            TargetSite = exception.TargetSite.Name;
            Type = exception.InnerException.GetType().FullName;
        }

        public LoggedException() { }

        public LoggedException(int id, string parameters, string message, string stackTrace, string source, string targetSite, string type, Guid errorNumber)
        {
            this.Parameters = parameters;
            this.Message = message;
            this.StackTrace = stackTrace;
            this.TargetSite = TargetSite;
            this.Type = type;
            this.ErrorNumber = errorNumber;
        }
    }
}
