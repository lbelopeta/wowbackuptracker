﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class UserAction
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public int ParentActionId { get; set; }
        [DataMember]
        public int? ClaimId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool GenerateMenuItem { get; set; }
        [DataMember]
        public string Code { get; set; }
    }
}
