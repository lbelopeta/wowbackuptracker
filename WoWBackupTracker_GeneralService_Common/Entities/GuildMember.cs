﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class GuildMember
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PrimarySpecializationName { get; set; }
        [DataMember]
        public string PrimarySpecializationRole { get; set; }
        [DataMember]
        public string SecondarySpecializationName { get; set; }
        [DataMember]
        public string SecondarySpecializationRole { get; set; }
        [DataMember]
        public int CharacterId { get; set; }
        [DataMember]
        public int GuildId { get; set; }
    }
}
