﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string BattleTag { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Email { get; set; }

        public User()
        { }
    }
}
