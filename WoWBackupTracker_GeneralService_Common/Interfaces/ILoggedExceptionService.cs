﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface ILoggedExceptionService
    {
        [OperationContract]
        LoggedException spInsLoggedException(LoggedException exception);
    }
}
