﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface IBlizzardAPIImport
    {
        [OperationContract]
        void ImportRealms(List<Realm> realms);
        [OperationContract]
        void ImportClasses(List<Class> classes);
    }
}
