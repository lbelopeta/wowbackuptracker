﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface IMenuService
    {
        [OperationContract]
        List<UserAction> GetUserActionsForMenu();

        [OperationContract]
        List<CharacterClaim> GetCharacterClaim(int userId, int? characterId);
    }
}
