﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface ICommonDataService
    {
        [OperationContract]
        List<Realm> GetRealms();

        [OperationContract]
        Realm InsertRealm(Realm realm);

        [OperationContract]
        List<Class> GetClasses();

        [OperationContract]
        List<SpecializationGridViewModel> GetSpecializations();

        [OperationContract]
        List<Specialization> GetSpecializationsforClass(int classId);
    }
}
