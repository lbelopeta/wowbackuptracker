﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WoWBT_Common.ExceptionHandling;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        User FindUserByUsername(string username);

        [OperationContract]
        User CreateUser(User user,string password);

        [OperationContract]
        User FindUserById(int id);

        [OperationContract]
        User Update(User user);
    }
}
