﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface ICharacterService
    {
        [OperationContract]
        Character InsertCharacter(Character character);

        [OperationContract]
        Character UpdateCharacter(Character character);

        [OperationContract]
        List<CharacterGridViewModel> GetAllUserCharacter(int userId);

        [OperationContract]
        Character GetCharacterById(int characterId);
    }
}
