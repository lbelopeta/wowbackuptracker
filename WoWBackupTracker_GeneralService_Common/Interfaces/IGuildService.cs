﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WoWBackupTracker.GeneralService
{
    [ServiceContract]
    public interface IGuildService
    {
        [OperationContract]
        Guild InsertGuild(Guild guild);

        [OperationContract]
        List<Guild> GetGuilds();

        [OperationContract]
        Guild GetGuildById(int id);

        [OperationContract]
        Guild UpdateGuild(Guild guild);

        [OperationContract]
        List<GuildMember> GetGuildMembers(int guildId);

        [OperationContract]
        GuildMember GetGuildMemberData(int characterId);

        [OperationContract]
        List<GuildForApplication> GetGuildsForApplication(int characterId);

        [OperationContract]
        void ApplyForGuild(int guildId, int characterID);

        [OperationContract]
        List<GuildApplicant> GetGuildApplicants(int guildId);

        [OperationContract]
        int? GetCharacterGuild(int characterId);

        [OperationContract]
        void ResolveGuildApplication(GuildApplicationResolution resolution);

    }
}
