﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;
using WoWBT_GeneralService_DAL;

namespace WoWBT.GeneralService
{
    public class CommonDataService : ServiceBase<CommonDataDAL>, ICommonDataService
    {
        public List<Class> GetClasses()
        {
            return this._dataAccessOperations.GetClasses();
        }

        public List<Realm> GetRealms()
        {
            return this._dataAccessOperations.GetRealms();
        }

        public List<SpecializationGridViewModel> GetSpecializations()
        {
            return this._dataAccessOperations.GetSpecializations();
        }

        public List<Specialization> GetSpecializationsforClass(int classId)
        {
            return this._dataAccessOperations.GetSpecializationsforClass(classId);
        }

        public Realm InsertRealm(Realm realm)
        {
            return this._dataAccessOperations.InsertRealm(realm);
        }
    }
}
