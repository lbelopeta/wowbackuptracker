﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;
using WowDotNetAPI;

namespace WoWBT.GeneralService
{
    public class BlizzardAPIImportService : ServiceBase<BlizzardAPIImportDAL>,IBlizzardAPIImport
    {
        public BlizzardAPIImportService()
        {
            this._apiClient = new BlizzardAPIClient();
        }

        private BlizzardAPIClient _apiClient;

        public void ImportRealms(List<Realm> realms)
        {
            var realmsFromAPI = this._apiClient.GetRealms();
            this._dataAccessOperations.ImportRealms(realmsFromAPI?.ToList());
        }

        public void ImportClasses(List<Class> classes)
        {
            var classesFromAPI = this._apiClient.GetClasses();
            this._dataAccessOperations.ImportClasses(classesFromAPI.ToList());
        }
    }
}
