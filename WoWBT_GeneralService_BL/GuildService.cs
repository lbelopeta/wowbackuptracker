﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;

namespace WoWBT.GeneralService
{
    public class GuildService : ServiceBase<GuildDAL>, IGuildService
    {
        public void ApplyForGuild(int guildId, int characterID)
        {
            this._dataAccessOperations.ApplyForGuild(guildId, characterID);
        }

        public int? GetCharacterGuild(int characterId)
        {
            return this._dataAccessOperations.GetCharacterGuild(characterId);
        }

        public List<GuildApplicant> GetGuildApplicants(int guildId)
        {
            return this._dataAccessOperations.GetGuildApplicants(guildId);
        }

        public Guild GetGuildById(int id)
        {
            return this._dataAccessOperations.GetGuildById(id);
        }

        public GuildMember GetGuildMemberData(int characterId)
        {
            return this._dataAccessOperations.GetGuildMemberData(characterId);
        }

        public List<GuildMember> GetGuildMembers(int guildId)
        {
            return this._dataAccessOperations.GetGuildMembers(guildId);
        }

        public List<Guild> GetGuilds()
        {
            return this._dataAccessOperations.GetGuilds();
        }

        public List<GuildForApplication> GetGuildsForApplication(int characterId)
        {
            return this._dataAccessOperations.GetGuildsForApplication(characterId);
        }

        public Guild InsertGuild(Guild guild)
        {
            return this._dataAccessOperations.InsertGuild(guild);
        }

        public void ResolveGuildApplication(GuildApplicationResolution resolution)
        {
            this._dataAccessOperations.ResolveGuildApplication(resolution);
        }

        public Guild UpdateGuild(Guild guild)
        {
            return this._dataAccessOperations.UpdateGuild(guild);
        }


    }
}
