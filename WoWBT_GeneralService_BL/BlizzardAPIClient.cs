﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WowDotNetAPI;

namespace WoWBT.GeneralService
{
    public class BlizzardAPIClient
    {
        private const string API_KEY = "64jx7mcu5g83rueee2hrxckdc4ub2kqu";
        private const Locale LOCALE = Locale.en_GB;
        private readonly WowExplorer explorer = new WowExplorer(Region.EU, LOCALE, API_KEY);
        public BlizzardAPIClient()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<WowDotNetAPI.Models.Realm, Realm>()
                 .ForMember(dest => dest.Type, source => source.MapFrom(r => r.Type.ToString())));

            Mapper.Initialize(cfg => cfg.CreateMap<WowDotNetAPI.Models.CharacterClassInfo, Class>()
                 .ForMember(dest => dest.ExternalId, source => source.MapFrom(r => r.Id)));
        }

        public IEnumerable<Realm> GetRealms()
        {
            var realms = explorer.GetRealms(LOCALE);
            return Mapper.Map<IEnumerable<WowDotNetAPI.Models.Realm>, IEnumerable<Realm>>(realms);

        }

        public IEnumerable<Class> GetClasses()
        {
            var classes = explorer.GetCharacterClasses();
            return Mapper.Map<IEnumerable<WowDotNetAPI.Models.CharacterClassInfo>, IEnumerable<Class>>(classes);
        }
    }
}
