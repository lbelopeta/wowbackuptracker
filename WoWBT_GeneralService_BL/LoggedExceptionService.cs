﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;

namespace WoWBT.GeneralService
{
    public class LoggedExceptionService : ServiceBase<LoggedExceptionDAL>,ILoggedExceptionService
    {
        public LoggedException spInsLoggedException(LoggedException exception)
        {
            return this._dataAccessOperations.spInsLoggedException(exception);
        }
    }
}
