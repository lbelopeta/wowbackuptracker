﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;
using WoWBT_GeneralService_DAL;

namespace WoWBT.GeneralService
{
    public class MenuService : ServiceBase<UserActionDAL>, IMenuService
    {
        public List<CharacterClaim> GetCharacterClaim(int userId, int? characterId)
        {
            return this._dataAccessOperations.GetCharacterClaim(userId, characterId);
        }

        public List<UserAction> GetUserActionsForMenu()
        {
            return this._dataAccessOperations.GetUserActionsForMenu();
        }
    }
}
