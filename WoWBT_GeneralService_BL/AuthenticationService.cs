﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;

namespace WoWBT.GeneralService
{
    public class AuthenticationService : ServiceBase<AuthenticationDAL>, IAuthenticationService
    {
        public User CreateUser(User user, string password)
        {
            return this._dataAccessOperations.CreateUser(user, password);
        }

        public User FindUserById(int id)
        {
            return this._dataAccessOperations.FindUserById(id);
        }

        public User FindUserByUsername(string username)
        {
            return this._dataAccessOperations.FindUserByUsername(username);
        }

        public User Update(User user)
        {
            return this._dataAccessOperations.Update(user);
        }
    }
}
