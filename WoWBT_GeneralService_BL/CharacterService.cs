﻿using System;
using System.Collections.Generic;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.Service;

namespace WoWBT.GeneralService
{
    public class CharacterService : ServiceBase<CharacterDAL>, ICharacterService
    {
        public List<CharacterGridViewModel> GetAllUserCharacter(int userId)
        {

            return this._dataAccessOperations.GetAllUserCharacter(userId);
        }

        public Character GetCharacterById(int characterId)
        {
            return this._dataAccessOperations.GetCharacterById(characterId);
        }

        public Character InsertCharacter(Character character)
        {
            return this._dataAccessOperations.InsertCharacter(character);
        }

        public Character UpdateCharacter(Character character)
        {
            return this._dataAccessOperations.UpdateCharacter(character);
        }
    }
}
