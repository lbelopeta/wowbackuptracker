﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;

namespace WoWBT.GeneralService
{
    public class CharacterDAL : DALBase, ICharacterService
    {
        public List<CharacterGridViewModel> GetAllUserCharacter(int userId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("UserId",userId)
            };

            return this._dataAccesOperations.FillObjectList<CharacterGridViewModel>(CommandType.StoredProcedure, "spSelAllUserCharacters", parameters.ToArray());
        }

        public Character GetCharacterById(int characterId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("CharacterId",characterId)
            };

            return this._dataAccesOperations.FillObject<Character>(CommandType.StoredProcedure, "spSelCharacterById", parameters.ToArray());
        }

        public Character InsertCharacter(Character character)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("Name",character.Name),
                new SqlParameter("ClassId",character.ClassId),
                new SqlParameter("PrimarySpecializationId",character.PrimarySpecializationId),
                new SqlParameter("SecondarySpecializationId",character.SecondarySpecializationId),
                new SqlParameter("RealmId",character.RealmId),
                new SqlParameter("UserId",character.UserId)
            };

            return this._dataAccesOperations.FillObject<Character>(CommandType.StoredProcedure, "spInsCharacter", parameters.ToArray());
        }

        public Character UpdateCharacter(Character character)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("Id",character.Id),
                new SqlParameter("ClassId",character.ClassId),
                new SqlParameter("PrimarySpecializationId",character.PrimarySpecializationId),
                new SqlParameter("SecondarySpecializationId",character.SecondarySpecializationId),
                new SqlParameter("RealmId",character.RealmId)
            };
            return this._dataAccesOperations.FillObject<Character>(CommandType.StoredProcedure, "spUpdCharacter", parameters.ToArray());

        }
    }
}
