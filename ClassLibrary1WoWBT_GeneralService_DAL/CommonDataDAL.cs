﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;

namespace WoWBT_GeneralService_DAL
{
    public class CommonDataDAL : DALBase, ICommonDataService
    {
        public List<Class> GetClasses()
        {
            return this._dataAccesOperations.FillObjectList<Class>(CommandType.StoredProcedure, "spSelClasses");
        }

        public List<Realm> GetRealms()
        {
            return this._dataAccesOperations.FillObjectList<Realm>(CommandType.StoredProcedure, "spSelRealms");
        }

        public List<SpecializationGridViewModel> GetSpecializations()
        {
            return this._dataAccesOperations.FillObjectList<SpecializationGridViewModel>(CommandType.StoredProcedure, "spSelSpecializations");
        }

        public List<Specialization> GetSpecializationsforClass(int classId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("ClassId",classId)
            };
            return this._dataAccesOperations.FillObjectList<Specialization>(CommandType.StoredProcedure, "spSelSpecializtionsForClass", parameters.ToArray());
        }

            

        public Realm InsertRealm(Realm realm)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("Name",realm.Name),
                new SqlParameter("Type",realm.Name)
            };

            return this._dataAccesOperations.FillObject<Realm>(CommandType.StoredProcedure, "spInsRealm", parameters.ToArray());
        }
    }
}
