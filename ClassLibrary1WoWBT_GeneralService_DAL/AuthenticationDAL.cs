﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;
using WoWBT_Common.ExceptionHandling;

namespace WoWBT.GeneralService
{
    public class AuthenticationDAL : DALBase, IAuthenticationService
    {
        public User CreateUser(User user,string password)
        {
            List<SqlParameter> parameters = new List<SqlParameter> {
                new SqlParameter("Username", user.UserName),
                new SqlParameter("Password", password),
                new SqlParameter("Email",user.Email)
        };
            return this._dataAccesOperations.FillObject<User>(CommandType.StoredProcedure, "spInsUser", parameters.ToArray());
        }

    public User FindUserById(int id)
    {
        List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("Id",id)
            };
        return this._dataAccesOperations.FillObject<User>(CommandType.StoredProcedure, "spSelUserById", parameters.ToArray());
    }

    public User FindUserByUsername(string username)
    {
        List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("Username",username)
            };
        return this._dataAccesOperations.FillObject<User>(CommandType.StoredProcedure, "spSelUserByUsername", parameters.ToArray());
    }

    public User Update(User user)
    {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("Id",user.Id),
                new SqlParameter("Battletag",user.BattleTag)
            };
            return this._dataAccesOperations.FillObject<User>(CommandType.StoredProcedure, "spUpdateUser", parameters.ToArray());
    }
}
}
