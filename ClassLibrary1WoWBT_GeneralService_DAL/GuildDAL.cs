﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;

namespace WoWBT.GeneralService
{
    public class GuildDAL : DALBase, IGuildService
    {
        public void ApplyForGuild(int guildId, int characterID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("GuildId",guildId),
                new SqlParameter("CharacterId", characterID)
            };

            this._dataAccesOperations.ExecuteNonQuery("spInsGuildApplication", parameters);
        }

        public int? GetCharacterGuild(int characterId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("CharacterId",characterId)
            };

            return _dataAccesOperations.ExecuteScalar<int?>(CommandType.StoredProcedure, "dbo.spSelCharacterGuild", parameters.ToArray());
        }

        public List<GuildApplicant> GetGuildApplicants(int guildId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("GuildId",guildId)
            };

            return this._dataAccesOperations.FillObjectList<GuildApplicant>(CommandType.StoredProcedure, "dbo.spSelGuildApplicants", parameters.ToArray());
        }

        public Guild GetGuildById(int id)
        {
            throw new NotImplementedException();
        }

        public GuildMember GetGuildMemberData(int characterId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("CharacterId",characterId)
            };

            return this._dataAccesOperations.FillObject<GuildMember>(CommandType.StoredProcedure, "spSelGuildMemberData", parameters.ToArray());
        }

        public List<GuildMember> GetGuildMembers(int guildId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("GuildId",guildId)
            };

            return this._dataAccesOperations.FillObjectList<GuildMember>(CommandType.StoredProcedure, "spSelGuildMembers", parameters.ToArray());
        }

        public List<Guild> GetGuilds()
        {
            throw new NotImplementedException();
        }

        public List<GuildForApplication> GetGuildsForApplication(int characterId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("CharacterId",characterId)
            };

            return this._dataAccesOperations.FillObjectList<GuildForApplication>(CommandType.StoredProcedure, "spSelGuildsForApplication", parameters.ToArray());
        }

        public Guild InsertGuild(Guild guild)
        {
            List<SqlParameter> paramaters = new List<SqlParameter>
            {
                new SqlParameter("Name",guild.Name),
                new SqlParameter("RealmId",guild.RealmId),
                new SqlParameter("OwnerId",guild.OwnerId)
            };

            return this._dataAccesOperations.FillObject<Guild>(CommandType.StoredProcedure, "spInsGuild", paramaters.ToArray());
        }

        public void ResolveGuildApplication(GuildApplicationResolution resolution)
        {

            List<SqlParameter> parameters = this._dataAccesOperations.CreateDbParameters(resolution);
            this._dataAccesOperations.ExecuteNonQuery("spResolveGuildApplication", parameters);
        }

        public Guild UpdateGuild(Guild guild)
        {
            throw new NotImplementedException();
        }
    }
}
