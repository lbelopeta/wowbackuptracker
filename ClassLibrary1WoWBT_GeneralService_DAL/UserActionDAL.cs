﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;

namespace WoWBT_GeneralService_DAL
{
    public class UserActionDAL : DALBase, IMenuService
    {
        public List<CharacterClaim> GetCharacterClaim(int userId, int? characterId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("UserId",userId),
                new SqlParameter("CharacterId",characterId)
            };

            return _dataAccesOperations.FillObjectList<CharacterClaim>(CommandType.StoredProcedure, "spSelCharacterClaims", parameters.ToArray());
        }

        public List<UserAction> GetUserActionsForMenu()
        {
            return this._dataAccesOperations.FillObjectList<UserAction>(CommandType.StoredProcedure, "spSelUserActionsForMenu", null);
        }
    }
}
