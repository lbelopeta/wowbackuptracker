﻿using Comminus.Experience.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;

namespace WoWBT.GeneralService
{
    public class LoggedExceptionDAL : DALBase, ILoggedExceptionService
    {
        public LoggedExceptionDAL()
        { }

        public LoggedException spInsLoggedException(LoggedException exception)
        {
            List<SqlParameter> parameters = new List<SqlParameter> {
                new SqlParameter("Type", exception.Type),
                new SqlParameter("Message", exception.Message),
                new SqlParameter("Parameters", exception.Parameters),
                new SqlParameter("StackTrace", exception.StackTrace),
                new SqlParameter("Source", exception.Source),
                new SqlParameter("TargetSite", exception.TargetSite)
                };

           return this._dataAccesOperations.FillObject<LoggedException>(CommandType.StoredProcedure, "spInsLoggedException", parameters.ToArray());
        }
    }
}
