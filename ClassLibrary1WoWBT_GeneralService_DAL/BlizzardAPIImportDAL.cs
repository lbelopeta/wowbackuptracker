﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBackupTracker.GeneralService;
using WoWBT_Common.DAL;
using WoWBT_Common.Helpers;

namespace WoWBT.GeneralService
{
    public class BlizzardAPIImportDAL : DALBase, IBlizzardAPIImport
    {
        public void ImportClasses(List<Class> classes)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter realmsParameter = new SqlParameter();
            realmsParameter.SqlDbType = SqlDbType.Structured;
            realmsParameter.ParameterName = ("@Classes");
            realmsParameter.Value = classes.ToDataTable();
            parameters.Add(realmsParameter);

            this._dataAccesOperations.FillObject<Class>(CommandType.StoredProcedure, "spInsClasses", parameters.ToArray());
        }

        public void ImportRealms(List<Realm> realms)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter realmsParameter = new SqlParameter();
            realmsParameter.SqlDbType = SqlDbType.Structured;
            realmsParameter.ParameterName = ("@Realms");
            realmsParameter.Value = realms.ToDataTable();
            parameters.Add(realmsParameter);

            this._dataAccesOperations.FillObject<Realm>(CommandType.StoredProcedure, "SpInsRealms", parameters.ToArray());
        }
    }
}
