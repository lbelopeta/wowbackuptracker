﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBT_Common.DAL;
using WoWBT_Common.ExceptionHandling;

namespace WoWBT_Common.Service
{
    [ExceptionHandling]
    public abstract class ServiceBase<DAL> where DAL : DALBase, new()
    {
        protected DAL _dataAccessOperations;

        public ServiceBase()
        {
            this._dataAccessOperations = new DAL();
        }
    }
}
