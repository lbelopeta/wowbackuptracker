﻿using Comminus.Experience.Data.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WoWBT_Common.Helpers
{
    public static class IListExtensions
    {
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));

            properties.Sort(new DeclarationOrderComparator());

            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                Type propType = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                DataAttribute dataArribs = (DataAttribute)prop.Attributes[typeof(DataAttribute)];
                if (dataArribs != null)
                {
                    if (dataArribs.AllowForAction == AllowForAction.None)
                        continue;
                }

                //enum fix...
                if (propType.BaseType == typeof(Enum))
                    propType = typeof(Int32);

                table.Columns.Add(prop.Name, propType);
            }

            if (data != null)
            {
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        DataAttribute dataArribs = (DataAttribute)prop.Attributes[typeof(DataAttribute)];
                        if (dataArribs != null)
                        {
                            if (dataArribs.AllowForAction == AllowForAction.None)
                                continue;
                        }

                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    }

                    table.Rows.Add(row);
                }
            }

            return table;
        }
    }

    public class DeclarationOrderComparator : IComparer
    {
        int IComparer.Compare(Object x, Object y)
        {
            PropertyInfo first = x as PropertyInfo;
            PropertyInfo second = y as PropertyInfo;
            if (first.MetadataToken < second.MetadataToken)
                return -1;
            else if (first.MetadataToken > second.MetadataToken)
                return 1;

            return 0;
        }
    }
}
