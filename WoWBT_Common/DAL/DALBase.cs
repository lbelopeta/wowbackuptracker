﻿using Comminus.Experience.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WoWBT_Common.ExceptionHandling;

namespace WoWBT_Common.DAL
{

    public abstract class DALBase
    {
        protected readonly DataAccess _dataAccesOperations;

        public DALBase()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            _dataAccesOperations = new DataAccess(connectionString);
        }
    }
}
