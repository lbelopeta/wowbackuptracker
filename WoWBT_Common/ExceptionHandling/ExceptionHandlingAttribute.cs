﻿using PostSharp.Aspects;
using PostSharp.Extensibility;
using PostSharp.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace WoWBT_Common.ExceptionHandling
{
    [MulticastAttributeUsage(Inheritance = MulticastInheritance.Multicast)]
    [Serializable]
    public class ExceptionHandlingAttribute : OnExceptionAspect
    {
        public ExceptionHandlingAttribute()
        {
            SemanticallyAdvisedMethodKinds = SemanticallyAdvisedMethodKinds.All;
        }

        public override sealed void OnException(MethodExecutionArgs args)
        {
            args.FlowBehavior = FlowBehavior.ThrowException;
            var exception = args.Exception;

            if (!(exception is MetaException) && !(exception is FaultException))
            {
                string parameters = CollectMethodParameters(args.Method, args.Method.GetParameters(), args.Arguments);
                MetaException metaException = new MetaException(exception.Message, exception, parameters);
                args.Exception = new FaultException<MetaException>(metaException, new FaultReason("aaaa"));
            }

        }

        private string CollectMethodParameters(MethodBase method, ParameterInfo[] parameters, Arguments arguments)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<call methodName='{0}'>", method.Name);
            sb.Append("<parameters>");

            for (int i = 0; i < arguments.Count; i++)
            {
                sb.AppendFormat("<parameter name='{0}' type='{1}' {2}>", parameters[i].Name, parameters[i].ParameterType.Name, arguments[i] == null ? "xsi:nil='true'/" : string.Empty);

                if (arguments[i] != null)
                {
                    string value = null;

                    try
                    {
                        if (arguments[i] is DataTable)
                        {
                            DataTable dt = (DataTable)arguments[i];
                            using (StringWriter sw = new StringWriter())
                            {
                                dt.WriteXmlSchema(sw);
                                dt.WriteXml(sw);

                                value = sw.ToString();
                            }
                        }
                        else if (arguments[i] is DataSet)
                        {
                            DataSet ds = (DataSet)arguments[i];
                            using (StringWriter sw = new StringWriter())
                            {
                                ds.WriteXmlSchema(sw);
                                ds.WriteXml(sw);

                                value = sw.ToString();
                            }
                        }
                        else
                        {
                            using (StringWriter sw = new StringWriter())
                            {
                                using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings() { OmitXmlDeclaration = true }))
                                {
                                    XmlSerializer x = new XmlSerializer(parameters[i].ParameterType);
                                    x.Serialize(writer, arguments[i]);
                                }

                                value = sw.ToString();
                            }
                        }
                    }
                    catch (Exception cantSerializeEx)
                    {
                        value = arguments[i].ToString();
                    }

                    sb.Append(value);

                    sb.Append("</parameter>");
                }

            }

            sb.Append("</parameters>");
            sb.Append("</call>");

            return sb.ToString();
        }
    }
}
