﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WoWBT_Common.ExceptionHandling
{
    [Serializable]
    public class MetaException : Exception,ISerializable
    {
        private readonly String parameters;

        public string Parameters { get { return this.parameters; } }

        public MetaException(string message, Exception exception, string parameters) : base(message, exception)
        {
            this.parameters = parameters;
        }

        public MetaException(string message, Exception exception) : base(message, exception)
        {

        }
        public MetaException() : base() { }
        public MetaException(string message) : base(message) { }

        protected MetaException(SerializationInfo info, StreamingContext context) : base(info, context) 
        {
            this.parameters = info.GetString("MetaaException.Parameters");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("MetaaException.Parameters", this.Parameters);
            base.GetObjectData(info, context);
        }

    }
}
