﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace UI.Identity
{
    public class CustomPasswordHasher : PasswordHasher
    {
        public string Username { get; set; }
        public override string HashPassword(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(Username + password);
            var hashBytes = System.Security.Cryptography.SHA256.Create().ComputeHash(bytes);
            var hashedPassword = Convert.ToBase64String(hashBytes);
            return hashedPassword;
        }

        public override PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            var hashedProvidedPassword = HashPassword(providedPassword);
            if (hashedPassword == hashedProvidedPassword)
            {
                return PasswordVerificationResult.Success;
            }
            return PasswordVerificationResult.Failed;
        }
    }
}