﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.AuthenticationService;
using UI.Models;
using WoWBackupTracker.GeneralService;

namespace UI.Identity
{
    public class UserInfo : IUser<int>
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string BattleTag { get; set; }

        public UserInfo() { }

        public UserInfo(User user)
        {
            this.Id = user.Id;
            this.UserName = user.UserName;
            this.Password = user.Password;
            this.Email = user.Email;
            this.BattleTag = user.BattleTag;
        }

        public UserInfo(EditUserViewModel user)
        {
            this.Id = user.Id;
            this.BattleTag = user.Battletag;
        }
    }
}