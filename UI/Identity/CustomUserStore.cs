﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using UI.Identity;
using UI.AuthenticationService;
using System.Security.Cryptography;
using System.Text;

namespace UI
{
    public class CustomUserStore : IUserStore<UserInfo, int>, IUserPasswordStore<UserInfo, int>
    {
        private IAuthenticationService _authenticationService;

        public Task CreateAsync(UserInfo user)
        {
            var service = GetAuthenticationService();
            WoWBackupTracker.GeneralService.User entity = new WoWBackupTracker.GeneralService.User();
            entity.UserName = user.UserName;
            entity.Email = user.Email;

            var result = service.CreateUser(entity, user.Password);
            var userWasCreated = result.Id != 0;
            return Task.FromResult(userWasCreated);
        }

        public Task DeleteAsync(UserInfo user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<UserInfo> FindByIdAsync(int userId)
        {
            var service = GetAuthenticationService();

            var user = service.FindUserById(userId);
            if (user != null)
            {
                UserInfo userInfo = new UserInfo(user);
                return Task.FromResult(userInfo);
            }
            return Task.FromResult<UserInfo>(null);
        }


        public Task<UserInfo> FindByNameAsync(string userName)
        {
            var service = GetAuthenticationService();
            var user = service.FindUserByUsername(userName);
            if (user != null)
            {
                UserInfo userInfo = new UserInfo(user);
                return Task.FromResult(userInfo);
            }
            return Task.FromResult<UserInfo>(null);
        }

        public Task<string> GetPasswordHashAsync(UserInfo user)
        {
            return Task.FromResult(user.Password);
        }

        public Task<bool> HasPasswordAsync(UserInfo user)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(UserInfo user, string passwordHash)
        {
            return Task.FromResult(user.Password = passwordHash);
        }

        public Task UpdateAsync(UserInfo user)
        {
            var a = 5;
            return Task.FromResult(false);
        }

        private UI.AuthenticationService.IAuthenticationService GetAuthenticationService()
        {
            if (this._authenticationService == null)
            {
                this._authenticationService = new AuthenticationServiceClient();
            }
            return this._authenticationService;
        }
    }
}