﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Text;

namespace UI.Identity
{
    public class CustomUserManager : UserManager<UserInfo, int>
    {
        public CustomUserManager(IUserStore<UserInfo, int> store) : base(store)
        {
            PasswordHasher = new CustomPasswordHasher();
        }

        public void SetUsername(string username)
        {
            if (PasswordHasher is CustomPasswordHasher)
            {
                var hasher = PasswordHasher as CustomPasswordHasher;
                hasher.Username = username;
            }
        }
    }
}