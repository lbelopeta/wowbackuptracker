﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace UI.Identity
{
    public static class IIdentityExtensions
    {
        public static string Email(this IIdentity identity)
        {
            return (identity as ClaimsIdentity).FirstOrNull(ClaimTypes.Email);
        }

        public static int Id(this IIdentity identity)
        {
            var idValue = (identity as ClaimsIdentity).FirstOrNull("ID");
            if (string.IsNullOrEmpty(idValue))
            {
                return 0;
            }

            int id;
            int.TryParse(idValue, out id);
            return id;
        }

        internal static string FirstOrNull(this ClaimsIdentity identity, string claimType)
        {
            var val = identity.FindFirst(claimType);
            return val == null ? null : val.Value;
        }
    }
}