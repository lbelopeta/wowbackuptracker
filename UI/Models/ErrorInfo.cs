﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UI.Models
{
    public class ErrorInfo
    {
        public HandleErrorInfo GenericErrorInfo { get; set; }
        public Guid ExceptionNumber { get; set; }
    }
}