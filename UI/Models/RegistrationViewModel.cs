﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class RegistrationViewModel
    {
        [Required]
        [EmailAddress]
        [UIHint("string")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required]
        [UIHint("string")]
        [DisplayName("Username")]
        public string Username { get; set; }

        [Required]
        [UIHint("password")]
        [DisplayName("Password")]
        [MinLength(6, ErrorMessage = "Password must have at least 6 characters.")]
        public string Password { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Password must have at least 6 characters.")]
        [EqualTo("Password", ErrorMessage = "Passwords do not match.")]
        [UIHint("password")]
        [DisplayName("Repeate password")]
        public string RepeatedPassword { get; set; }
    }
}