﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class EditCharacterViewModel : CharacterViewModel
    {
        [Required]
        public int Id { get; set; }
    }
}