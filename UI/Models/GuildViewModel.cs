﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class GuildViewModel
    {
        public string Name { get; set; }
        public int RealmId { get; set; }
    }
}