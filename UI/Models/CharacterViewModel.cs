﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class CharacterViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "You must select class.")]
        public int ClassId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "You must select primary specialization.")]
        public int PrimarySpecializationId { get; set; }
        [NotEqualTo("PrimarySpecializationId", ErrorMessage = "Your secondery specialization can't be same as primary specialization")]
        public int? SecondarySpecializationId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "You must select realm.")]
        public int RealmId { get; set; }
    }
}