﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UI.Identity;

namespace UI.Models
{
    public class EditUserViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        [UIHint("string")]
        public string Battletag { get; set; }

        public EditUserViewModel()
        {

        }

        public EditUserViewModel(UserInfo user)
        {
            this.Id = user.Id;
            this.Username = user.UserName;
            this.Battletag = user.BattleTag;
            this.Email = user.Email;
        }
    }
}