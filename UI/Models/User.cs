﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class User : IUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}