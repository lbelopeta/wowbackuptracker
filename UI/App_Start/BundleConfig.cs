﻿using System.Web.Optimization;

namespace UI.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/Scripts/jquery").Include(
                "~/Scripts/jquery-1.10.2.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/mvcfoolproof.unobtrusive.min.js",
                "~/Scripts/jquery.cookie.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/master").Include(
                "~/Scripts/Custom/masterPage.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/Custom").Include(
                "~/Scripts/Custom/character.js",
                "~/Scripts/Custom/guildApplication.js"
                ));
        }

    }
}