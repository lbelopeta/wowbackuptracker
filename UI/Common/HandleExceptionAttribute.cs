﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Web.Mvc;
using UI.GeneralService;
using WoWBT_Common.ExceptionHandling;

namespace UI.Common
{
    public class HandleExceptionAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var exception = filterContext.Exception;
            var b = exception.GetType();

            LoggedExceptionServiceClient service = new LoggedExceptionServiceClient();
            LoggedException data;
            SqlException sqlException = null;


            if (exception is MetaException)
            {
                var metaExcpetion = exception as MetaException;
                data = new LoggedException()
                {
                    Parameters = metaExcpetion.Parameters,
                    Message = metaExcpetion.Message,
                    StackTrace = metaExcpetion.StackTrace,
                    Source = metaExcpetion.Source,
                    TargetSite = metaExcpetion.TargetSite.Name,
                    Type = metaExcpetion.InnerException.GetType().FullName
                };
                if (metaExcpetion.InnerException is SqlException)
                {
                    sqlException = (metaExcpetion.InnerException as SqlException);
                }
            }
            else
            {
                data = new LoggedException();
                data.Message = exception.Message;
                data.Parameters = "Params not available";
                data.Source = exception.Source;
                data.StackTrace = exception.StackTrace;
                data.TargetSite = exception.TargetSite.Name;
                data.Type = exception.GetType().Name;
                if (exception is SqlException)
                {

                    sqlException = (exception as SqlException);
                }
            }

            data = service.spInsLoggedException(data);

            var result = CreateActionResult(filterContext, data.ErrorNumber, sqlException);
            filterContext.Result = result;

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        protected virtual ActionResult CreateActionResult(ExceptionContext filterContext, Guid errorNumber, SqlException exception)
        {
            if (exception == null)
            {
                return CreateActionResult(filterContext, errorNumber);
            }

            var ctx = new ControllerContext(filterContext.RequestContext, filterContext.Controller);

            var viewName = filterContext.RouteData.Values["action"] as string;

            var result = new ViewResult
            {
                ViewName = viewName,
                ViewData = new ViewDataDictionary()
            };
            result.ViewData.Add("SQLError", exception.Message);

            return result;
        }

        protected virtual ActionResult CreateActionResult(ExceptionContext filterContext, Guid errorNumber)
        {
            var ctx = new ControllerContext(filterContext.RequestContext, filterContext.Controller);

            var viewName = "~/Views/Error/Index.cshtml";

            var result = new ViewResult
            {
                ViewName = viewName,
                ViewData = new ViewDataDictionary(),
            };
            result.ViewData.Add("ErrorNumber", errorNumber);

            return result;
        }

        protected string SelectFirstView(ControllerContext ctx, params string[] viewNames)
        {
            return viewNames.First(view => ViewExists(ctx, view));
        }

        protected bool ViewExists(ControllerContext ctx, string name)
        {
            var result = ViewEngines.Engines.FindView(ctx, name, null);
            return result.View != null;
        }

    }
}