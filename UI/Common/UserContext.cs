﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.MenuService;
using System.Web.SessionState;
using UI.Identity;
using UI.GuildService;

namespace UI.Common
{
    public class UserContext
    {
        private readonly static IMenuService _service = new MenuServiceClient();
        private readonly static IGuildService _guildService = new GuildServiceClient();

        private const string SELECTED_CHARACTER = "UserContext.SelectedCharacter";
        private const string ALLOWED_USER_ACTIONS = "UserContext.AllowedUserActions";
        private const string USER_ACTIONS = "UserContext.UserActions";
        private const string USER_GUILD = "UserContext.GuildID";

        private static HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }

        }


        private List<CharacterClaim> GetClaims()
        {
            return null;
        }

        public static int SelectedCharacter
        {
            get
            {
                var selectedCharacter = Session[SELECTED_CHARACTER];
                if (selectedCharacter == null)
                {
                    return 0;
                }

                int selectedCharacterId = 0;
                int.TryParse(selectedCharacter.ToString(), out selectedCharacterId);
                return selectedCharacterId;
            }
            set
            {
                Session[SELECTED_CHARACTER] = value;
            }
        }

        public static List<UserAction> AllowedMenuUserActions
        {
            get
            {
                return AllowedUserActions.Where(p => p.GenerateMenuItem).ToList();
            }
        }

        public static List<UserAction> AllowedUserActions
        {
            get
            {
                var items = Session[ALLOWED_USER_ACTIONS];
                if (items == null || !(items is List<UserAction>))
                {
                    return new List<UserAction>();
                }

                return (items as List<UserAction>);

            }
        }

        public static int? Guild
        {
            get
            {
                return (int?)Session[USER_GUILD];
            }
            set
            {
                Session[USER_GUILD] = value;
            }
        }

        public static IEnumerable<UserAction> GetAllUserActions()
        {
            var userActions = Session[USER_ACTIONS];
            if (userActions == null)
            {
                _service.GetUserActionsForMenu();
                userActions = _service.GetUserActionsForMenu();
                Session[USER_ACTIONS] = userActions;
            }

            return userActions as IEnumerable<UserAction>;
        }



        public static void SetUserContext()
        {
            List<CharacterClaim> characterCalims = GetCharacetClaims();
            var claimIds = characterCalims.Select(p => p.Id).ToList();
            SetAllowedUserActions(claimIds);
            SetGuild();
        }

        public static void SetUserContext(int userId)
        {
            List<CharacterClaim> characterCalims = GetCharacetClaims(userId);
            var claimIds = characterCalims.Select(p => p.Id).ToList();
            SetAllowedUserActions(claimIds);
            SetGuild();
        }

        private static void SetGuild()
        {
            Guild = _guildService.GetCharacterGuild(SelectedCharacter);
        }

        private static void SetAllowedUserActions(List<int> characterCalims)
        {
            var userActions = GetAllUserActions();
            Session[ALLOWED_USER_ACTIONS] = userActions.Where(p => !p.ClaimId.HasValue || characterCalims.Contains(p.ClaimId.Value)).ToList();
        }

        private static List<CharacterClaim> GetCharacetClaims()
        {
            var userId = HttpContext.Current.User.Identity.Id();
            return GetCharacetClaims(userId);
        }

        private static List<CharacterClaim> GetCharacetClaims(int id)
        {
            return _service.GetCharacterClaim(id, SelectedCharacter);
        }
    }
}