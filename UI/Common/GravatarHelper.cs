﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using UI.Identity;

namespace UI.Common
{
    public class GravatarHelper
    {
        #region Constants
        private const string BASE_URL = "https://www.gravatar.com/avatar/";
        /// <summary>
        /// Image whihc will be used in case user has no gravatar account.
        /// Must be html escaped.
        /// </summary>
        private const string DEFUALT_IMGAE = "https%3A%2F%2Fencrypted-tbn0.gstatic.com%2Fimages%3Fq%3Dtbn%3AANd9GcQSekw4fz6jnEdxKkPnVA4kDsWth-pFuMgj2uZTwMFChcEZW5Cd";
        private const int MAX_SIZE = 40;
        #endregion

        private static string GetUserEmail()
        {
            return HttpContext.Current.User.Identity.Email();
        }

        private static string GetMD5Hash(string textToHash)
        {
            //Check wether data was passed
            if (string.IsNullOrEmpty(textToHash))
            {
                return string.Empty;
            }

            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(textToHash));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public static string GetGravatarURL()
        {
            string email = GetUserEmail();
            string hashedEmail = GetMD5Hash(email);
            return string.Format("{0}{1}?d={2}&s={3}", BASE_URL, hashedEmail, DEFUALT_IMGAE,MAX_SIZE);
        }
    }
}