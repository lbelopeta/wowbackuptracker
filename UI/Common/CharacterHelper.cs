﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.CharacterService;
using UI.GuildService;
using UI.Identity;

namespace UI.Common
{
    public class CharacterHelper
    {
        private static List<CharacterGridViewModel> GetAllUserCharactersFromDB()
        {
            ICharacterService characterService = new CharacterServiceClient();
            int id = GetUserId();
            return characterService.GetAllUserCharacter(id);
        }

        public static List<SelectListItem> GetCharacterDropdownItems()
        {
            var characters = GetAllUserCharactersFromDB();
            var selectedCharacter = GetSelectedCharacter();

            var dropDownItems = characters.Select(p => new SelectListItem()
            {
                Value = p.Id.ToString(),
                Text = p.Name,
                Selected = (p.Id == selectedCharacter)
            }).ToList();

            if (selectedCharacter == 0)
            {
                dropDownItems.Insert(0, new SelectListItem() { Text = " -- select character --", Value = "0", Selected = true });
            }
            return dropDownItems;
        }

        public static int GetCharacterGuildId()
        {
            IGuildService guildService = new GuildServiceClient();
            int characterId = GetSelectedCharacter();
            int guildId;

            var guildMember = guildService.GetGuildMemberData(characterId);
            if (guildMember != null)
            {
                guildId = guildMember.GuildId;
            }
            else
            {
                guildId = 0;
            }
            return guildId;
        }

        public static int GetSelectedCharacter()
        {
            return UserContext.SelectedCharacter;
        }

        private static int GetUserId()
        {
            return HttpContext.Current.User.Identity.Id();
        }
    }
}