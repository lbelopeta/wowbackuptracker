﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace UI.Common
{
    public class CustomAuthorizationFilter : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = true;
            var action = GetCurrentAction();

            authorize = UserContext.AllowedUserActions.Select(p => p.Action).Contains(action);

            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }

        public string GetCurrentAction()
        {
            HttpContext httpContext = HttpContext.Current;

            string area = string.Empty;
            if (httpContext.Request.RequestContext.RouteData.Values.ContainsKey("area"))
                area = httpContext.Request.RequestContext.RouteData.GetRequiredString("area").ToString();
            else if (httpContext.Request.RequestContext.RouteData.DataTokens.ContainsKey("area"))
                area = httpContext.Request.RequestContext.RouteData.DataTokens["area"].ToString();

            string action =
                (!String.IsNullOrEmpty(area) ? ("/" + area) : String.Empty) +
                (httpContext.Request.RequestContext.RouteData.Values.ContainsKey("controller") ? "/" + httpContext.Request.RequestContext.RouteData.GetRequiredString("controller").ToString() : String.Empty) +
                (httpContext.Request.RequestContext.RouteData.Values.ContainsKey("action") ? "/" + httpContext.Request.RequestContext.RouteData.GetRequiredString("action").ToString() : String.Empty);

            return action;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ||
                                        filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

            if (skipAuthorization)
            {
                return;
            }
            base.OnAuthorization(filterContext);
        }
    }
}