﻿$(function () {
    $("#ClassId").on('change', function () {
        var selectedValue = this.value;
        var primarySpecializationDropdown = $('#PrimarySpecializationId');
        var secondarySpecializationDropdown = $('#SecondarySpecializationId');

        $.get('GetSpecializationsForClass?classId=' + selectedValue, function (data) {
            //remove all items from both dropdowns
            primarySpecializationDropdown.empty();
            secondarySpecializationDropdown.empty();

            //add placeholder values and set them as selected items
            primarySpecializationDropdown.append($("<option></option>")
                        .attr("value", '0').text('-- select value --'));
            secondarySpecializationDropdown.append($("<option></option>")
                    .attr("value", '').text('-- select value --'));

            primarySpecializationDropdown.val('0');
            secondarySpecializationDropdown.val('0');

            $.each(data, function (i, item) {
                primarySpecializationDropdown.append($("<option></option>")
                        .attr("value", item.value).text(item.name));
                secondarySpecializationDropdown.append($("<option></option>")
                        .attr("value", item.value).text(item.name));
            });
        });
    });
});