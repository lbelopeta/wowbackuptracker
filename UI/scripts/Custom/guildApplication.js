﻿$($('.js-handle-apply').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    var guildId = $this.attr('data-guild-id');
    $.ajax({
        type: 'POST',
        url: "/GuildApplication/Apply",
        data: { GuildID: guildId },
        success: function (data) {
            window.location.reload(true);
        }
    });
}));