﻿$(function () {
    $('#currentCharacter').change(function () {
        var $this = $(this);
        var selectedValue = $this.val();
        $.post("/Character/ChangeSelectedCharacter", { characterId: selectedValue });
        window.location.reload();
    });
});