﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.CommonDataService;

namespace UI.Controllers
{
    public class RealmController : ControllerBase
    {
        private ICommonDataService _service;
        public RealmController(ICommonDataService service)
        {
            this._service = service;
        }

        // GET: CommonData
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Realms()
        {
            return View();
        }


        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = this._service.GetRealms();
            return PartialView("_GridViewPartial", model);
        }

        //[HttpPost, ValidateInput(false)]
        //public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] Realm item)
        //{
        //    var model = this._service.GetRealms();
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            this._service.InsertRealm(item);
        //        }
        //        catch (Exception e)
        //        {
        //            ViewData["EditError"] = e.Message;
        //        }
        //    }
        //    else
        //        ViewData["EditError"] = "Please, correct all errors.";
        //    return PartialView("_GridViewPartial", model);
        //}
    }
}