﻿using DevExpress.Web.Mvc;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.CharacterService;
using UI.CommonDataService;
using UI.Models;
using UI.Identity;
using UI.Common;

namespace UI.Controllers
{
    public class CharacterController : ControllerBase
    {
        private ICommonDataService _commonDataService;
        private ICharacterService _characterService;

        public CharacterController(ICommonDataService commonDataService, ICharacterService characterService)
        {
            this._commonDataService = commonDataService;
            this._characterService = characterService;
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CharacterViewModel, Character>();
                cfg.CreateMap<Character, EditCharacterViewModel>();
                cfg.CreateMap<EditCharacterViewModel, Character>();

            });
        }
        // GET: Character
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            SetDropdownValues();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CharacterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                SetDropdownValues(model.ClassId);
                return View(model);
            }
            var entity = Mapper.Map<CharacterViewModel, Character>(model);
            entity.UserId = User.Identity.Id();
            this._characterService.InsertCharacter(entity);
            return RedirectToAction("Index");
        }
        [Route("Character/Edit/{id}")]
        public ActionResult Edit(int id)
        {
            var entity = this._characterService.GetCharacterById(id);
            var model = Mapper.Map<Character, EditCharacterViewModel>(entity);
            SetDropdownValues(model.ClassId);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditCharacterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var entity = Mapper.Map<EditCharacterViewModel, Character>(model);
                this._characterService.UpdateCharacter(entity);
                return RedirectToAction("Index");
            }

            SetDropdownValues(model.ClassId);
            return View(model);
        }

        private void SetDropdownValues(int? classId = null)
        {
            List<SelectListItem> classDropDownItems;
            var classes = this._commonDataService.GetClasses();
            classDropDownItems = classes.
                Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }).ToList();
            classDropDownItems.Insert(0, new SelectListItem() { Text = "--select value--", Selected = true, Value = "0" });

            List<SelectListItem> realmDropDownItems;
            var realms = this._commonDataService.GetRealms();
            realmDropDownItems = realms.Select(r => new SelectListItem() { Text = r.Name, Value = r.Id.ToString() }).ToList();
            realmDropDownItems.Insert(0, new SelectListItem() { Text = "--select value--", Selected = true, Value = "0" });

            List<SelectListItem> specializationDropDownItems = new List<SelectListItem>();
            specializationDropDownItems.Add(new SelectListItem() { Text = "--select value--", Selected = true, Value = "" });
            if (classId != null && classId != 0)
            {
                var specializations = this._commonDataService.GetSpecializationsforClass(classId.Value);
                specializationDropDownItems.AddRange(specializations.Select(r => new SelectListItem() { Text = r.Name, Value = r.Id.ToString() }));
            }

            ViewBag.ClassItems = classDropDownItems;
            ViewBag.RealmItems = realmDropDownItems;
            ViewBag.SpecializationItems = specializationDropDownItems;
            ViewBag.SecondarySpecializationItems = specializationDropDownItems.ConvertAll(item => new SelectListItem() { Selected = item.Selected, Text = item.Text, Value = item.Value });
        }

        public JsonResult GetSpecializationsForClass(int classID)
        {
            var specializations = this._commonDataService.GetSpecializationsforClass(classID);
            return Json(specializations.Select(p => new { value = p.Id, name = p.Name }), JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = this._characterService.GetAllUserCharacter(User.Identity.Id());
            return PartialView("_GridViewPartial", model);
        }

        [HttpPost]
        public ActionResult ChangeSelectedCharacter(SelectedCharacterChangeModel model)
        {
            UserContext.SelectedCharacter = model.CharacterId;
            UserContext.SetUserContext();
            return null;
        }
    }
}