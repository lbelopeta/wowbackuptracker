﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Common;
using UI.MenuService;

namespace UI.Controllers
{
    public class MenuController : Controller
    {

        public ActionResult Menu()
        {
            Dictionary<int,List<UserAction>> menuItems = UserContext.AllowedMenuUserActions.GroupBy( p => p.ParentActionId).ToDictionary(key => key.Key, key => key.ToList());
            return PartialView(menuItems);
        }
    }
}