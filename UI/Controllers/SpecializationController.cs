﻿//using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.CommonDataService;

namespace UI.Controllers
{
    public class SpecializationController : ControllerBase
    {
        private ICommonDataService _service;

        public SpecializationController(ICommonDataService service)
        {
            this._service = service;
        }
        public ActionResult Index()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = this._service.GetSpecializations();
            return PartialView("_GridViewPartial", model);
        }
    }
}