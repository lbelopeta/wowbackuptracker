﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UI.Common;
using UI.Identity;
using UI.Models;
using WoWBackupTracker.GeneralService;

namespace UI.Controllers
{
    public class AccountController : Controller
    {

        public AccountController(UI.AuthenticationService.IAuthenticationService service)
        {
            this._service = service;
            this.UserManager = new CustomUserManager(new CustomUserStore());
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        private UI.AuthenticationService.IAuthenticationService _service;

        public UserManager<UserInfo, int> UserManager { get; set; }

        #region Register, Log on and log off

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserInfo userInfo = new UserInfo();
                userInfo.UserName = model.Username;
                userInfo.Email = model.Email;

                ((CustomUserManager)UserManager).SetUsername(model.Username);

                var result = await UserManager.CreateAsync(userInfo, model.Password);
                if (result.Succeeded)
                {
                    await SignInAsync(userInfo, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            var a = this.User;
            if (ModelState.IsValid)
            {
                ((CustomUserManager)UserManager).SetUsername(model.Username);

                var user = await UserManager.FindAsync(model.Username, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, false);
                    UserContext.SetUserContext(user.Id);

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    return RedirectToAction(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
        #endregion
        [Authorize]
        public async Task<ActionResult> Edit()
        {
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            var model = new EditUserViewModel(user);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User();
                user.Id = model.Id;
                user.BattleTag = model.Battletag;
                this._service.Update(user);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        #region Helpers


        private async Task SignInAsync(UserInfo user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            identity.AddClaim(new Claim("ID", user.Id.ToString()));
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion
    }
}