﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.BlizzardApiService;

namespace UI.Controllers
{
    public class AdministrationController : ControllerBase
    {
        private IBlizzardAPIImport _service;

        public AdministrationController(IBlizzardAPIImport service)
        {
            this._service = service;
        }
        // GET: Administration
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportRealms()
        {
            this._service.ImportRealms(null);
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportClasses()
        {
            this._service.ImportClasses(null);
            return View("Index");
        }
    }
}