﻿using DevExpress.Web.Mvc;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.CommonDataService;
using UI.GuildService;
using UI.Models;
using UI.Identity;
using UI.Common;

namespace UI.Controllers
{
    public class GuildController : ControllerBase
    {
        public IGuildService _guildService;
        public ICommonDataService _commonDataService;

        public GuildController(IGuildService guildService, ICommonDataService commonDAtaService)
        {
            this._guildService = guildService;
            this._commonDataService = commonDAtaService;
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<GuildViewModel, Guild>();
            });
        }
        // GET: Guild
        public ActionResult Index()
        {
            var guildId = CharacterHelper.GetCharacterGuildId();
            if(guildId == 0)
            {
                ViewBag.Error = string.Format("Character is not member of any guild");
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Create()
        {
            var characterGuildId = CharacterHelper.GetCharacterGuildId();
            if (characterGuildId != 0)
            {
                return RedirectToAction("Index");
            }
            SetDropdownValues();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GuildViewModel model)
        {
            var characterId = CharacterHelper.GetSelectedCharacter();
            if (ModelState.IsValid && characterId != 0)
            {
                var entity = Mapper.Map<GuildViewModel, Guild>(model);
                entity.OwnerId = characterId;
                this._guildService.InsertGuild(entity);
                return RedirectToAction("Index");
            }

            SetDropdownValues();
            if(characterId == 0)
            {
                ModelState.AddModelError("", "You must select character");
            }
            return View(model);
        }

        private void SetDropdownValues()
        {
            List<SelectListItem> realmDropDownItems;
            var realms = this._commonDataService.GetRealms();
            realmDropDownItems = realms.Select(r => new SelectListItem() { Text = r.Name, Value = r.Id.ToString() }).ToList();
            realmDropDownItems.Insert(0, new SelectListItem() { Text = "--select value--", Selected = true, Value = "0" });

            ViewBag.RealmItems = realmDropDownItems;
        }



        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = _guildService.GetGuildMembers(CharacterHelper.GetCharacterGuildId());
            return PartialView("_GridViewPartial", model);
        }
    }
}