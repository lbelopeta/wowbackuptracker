﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Common;
using UI.GuildService;

namespace UI.Controllers
{
    public class GuildApplicantsController : ControllerBase
    {
        private readonly IGuildService _service;
        public GuildApplicantsController(IGuildService service)
        {
            this._service = service;
        }

        public ActionResult Index()
        {
            var model = _service.GetGuildApplicants(UserContext.Guild ?? 0);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Accept(GuildApplicationResolution model)
        {
            if (ModelState.IsValid)
            {
                model.Accepted = true;
                this._service.ResolveGuildApplication(model);
            }

            return RedirectToAction("Index", "Guild");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Decline(GuildApplicationResolution model)
        {
            if (ModelState.IsValid)
            {
                model.Accepted = false;
                this._service.ResolveGuildApplication(model);
            }

            return RedirectToAction("Index", "Guild");
        }
    }
}