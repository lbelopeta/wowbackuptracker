﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Common;
using UI.GuildService;
using UI.Models;

namespace UI.Controllers
{
    public class GuildApplicationController : ControllerBase
    {
        private IGuildService _guildService;

        public GuildApplicationController(IGuildService guildService)
        {
            this._guildService = guildService;
        }
        // GET: GuildApplication
        public ActionResult Index()
        {
            int characterGuildId = CharacterHelper.GetCharacterGuildId();
            if(characterGuildId != 0)
            {
                return RedirectToAction("Index", "Guild");
            }
            return View();
        }

        [ValidateInput(false)]
        public ActionResult GridView1Partial()
        {
            int characterId = CharacterHelper.GetSelectedCharacter();
            var model = this._guildService.GetGuildsForApplication(characterId);
            return PartialView("_GridView1Partial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Apply(ApplyForGuildModel model)
        {
            var characterId = CharacterHelper.GetSelectedCharacter();
            this._guildService.ApplyForGuild(model.GuildId, characterId);
            return Json(new { success = true });
        }
    }
}