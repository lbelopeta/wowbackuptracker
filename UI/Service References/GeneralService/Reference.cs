﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UI.GeneralService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="LoggedException", Namespace="http://schemas.datacontract.org/2004/07/WoWBackupTracker.GeneralService")]
    [System.SerializableAttribute()]
    public partial class LoggedException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Guid ErrorNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ParametersField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SourceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StackTraceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TargetSiteField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Guid ErrorNumber {
            get {
                return this.ErrorNumberField;
            }
            set {
                if ((this.ErrorNumberField.Equals(value) != true)) {
                    this.ErrorNumberField = value;
                    this.RaisePropertyChanged("ErrorNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Parameters {
            get {
                return this.ParametersField;
            }
            set {
                if ((object.ReferenceEquals(this.ParametersField, value) != true)) {
                    this.ParametersField = value;
                    this.RaisePropertyChanged("Parameters");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Source {
            get {
                return this.SourceField;
            }
            set {
                if ((object.ReferenceEquals(this.SourceField, value) != true)) {
                    this.SourceField = value;
                    this.RaisePropertyChanged("Source");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StackTrace {
            get {
                return this.StackTraceField;
            }
            set {
                if ((object.ReferenceEquals(this.StackTraceField, value) != true)) {
                    this.StackTraceField = value;
                    this.RaisePropertyChanged("StackTrace");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TargetSite {
            get {
                return this.TargetSiteField;
            }
            set {
                if ((object.ReferenceEquals(this.TargetSiteField, value) != true)) {
                    this.TargetSiteField = value;
                    this.RaisePropertyChanged("TargetSite");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Type {
            get {
                return this.TypeField;
            }
            set {
                if ((object.ReferenceEquals(this.TypeField, value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="GeneralService.ILoggedExceptionService")]
    public interface ILoggedExceptionService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ILoggedExceptionService/spInsLoggedException", ReplyAction="http://tempuri.org/ILoggedExceptionService/spInsLoggedExceptionResponse")]
        UI.GeneralService.LoggedException spInsLoggedException(UI.GeneralService.LoggedException exception);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ILoggedExceptionService/spInsLoggedException", ReplyAction="http://tempuri.org/ILoggedExceptionService/spInsLoggedExceptionResponse")]
        System.Threading.Tasks.Task<UI.GeneralService.LoggedException> spInsLoggedExceptionAsync(UI.GeneralService.LoggedException exception);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ILoggedExceptionServiceChannel : UI.GeneralService.ILoggedExceptionService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class LoggedExceptionServiceClient : System.ServiceModel.ClientBase<UI.GeneralService.ILoggedExceptionService>, UI.GeneralService.ILoggedExceptionService {
        
        public LoggedExceptionServiceClient() {
        }
        
        public LoggedExceptionServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public LoggedExceptionServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public LoggedExceptionServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public LoggedExceptionServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public UI.GeneralService.LoggedException spInsLoggedException(UI.GeneralService.LoggedException exception) {
            return base.Channel.spInsLoggedException(exception);
        }
        
        public System.Threading.Tasks.Task<UI.GeneralService.LoggedException> spInsLoggedExceptionAsync(UI.GeneralService.LoggedException exception) {
            return base.Channel.spInsLoggedExceptionAsync(exception);
        }
    }
}
